//
//  LoginViewModel.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/2/7.
//  Copyright © 2020 TCIT. All rights reserved.
//

import Foundation
import Moya
import RxMoya
import RxSwift
import RxCocoa

enum LoginResult {
    case Successed
    case Loading
    case NoData
    case NoMatch
}

// https://beeth0ven.github.io/RxSwift-Chinese-Documentation/content/rxswift_core/observable/single.html

final class LoginViewModel: BaseViewModel {
    
    private let apiManager = ApiManager.shared
    private let accountManager = AccountManager.shared
    
    var userList: [UserData]?
    var isFetching = false
    
    private func tryLogin(_ account: String, _ password: String, usedCached: Bool) -> Single<(LoginResult, String)> {
        if (usedCached) {
            let cacheLoginResult = getCacheloginResult(account, password)
            
            if LoginResult.NoData == cacheLoginResult.0 {
                let serial = ApiConfig.getApiToken()
                return apiManager.fetchUsers(serial: serial)
                    .map({ userArray in
                        self.userList = userArray
                        self.isFetching = false
                        return self.getCacheloginResult(account, password)
                    })
            } else {
                return Single.just((cacheLoginResult.0, cacheLoginResult.1))
                
                
            }
        } else {
            let serial = ApiConfig.getApiToken()
            return apiManager.fetchUsers(serial: serial)
            .map({ userArray in
                if let userArray = userArray {
                    if (userArray.count == 0) {
                        return (LoginResult.NoData, "請稍候")
                    } else if self.checkMemberThenStored(userList: userArray, account: account, password: password) {
                        return (LoginResult.Successed, "登入成功")
                    } else {
                        return (LoginResult.NoMatch, "帳號密碼錯誤")
                    }
                } else {
                    return (LoginResult.NoData, "請稍候")
                }
            })
        }
        
        /*
        let singleJob = Single<(LoginResult, String)>.create { single in
            single(.success((cacheLoginResult.0, cacheLoginResult.1)))
            return Disposables.create()
        }
        return singleJob
        */
    }
    
    private func checkMemberThenStored(userList: [UserData], account: String, password: String) -> Bool {
        for i in 0..<userList.count {
            let user = userList[i]
            if (user.schoolNo == account) && (user.password == password) {
                accountManager.storedUserLogin(account, password)
                accountManager.storedUserInfomation(user)
                return true
            }
        }
        
        return false
    }
    
    private func getCacheloginResult(_ account: String, _ password: String) -> (LoginResult, String) {
        if (isFetching) {
            return (LoginResult.Loading, "載入中")
        }
        
        if let userList = self.userList {
            if (self.checkMemberThenStored(userList: userList, account: account, password: password)) {
                return (LoginResult.Successed, "登入成功")
            } else {
                return (LoginResult.NoMatch, "帳號密碼錯誤")
            }
        } else {
            return (LoginResult.NoData, "請稍候")
        }
    }
    
    // MARK: deprecated
    private func fetchUserListToLogin(_ account: String, _ password: String) {
        let serial = ApiConfig.getApiToken()
        
        apiManager.request(AiStoreApi.SendSerialNo(serial))
            .delay(3, scheduler: MainScheduler.instance)
            .flatMap { _ in
                return self.apiManager.requestReturnDecodable(AiStoreApi.ReceiveCurrentPerson(serial)) }
            .do(onSuccess: { _ in
                self.isFetching = false
            }, onSubscribe: {
                self.isFetching = true
                self.progressingPublish.onNext(true)
            }, onDispose: {
                self.isFetching = false
                self.progressingPublish.onNext(false)
            })
            .subscribe{ result in
                switch result {
                case let .success(response):
                    if (response.count > 0) {
                        self.userList = response[0].userArray
                        
                        // 用底線消除警告
                        //_ = self.tryLogin(account, password)
                    }
                    break
                case let .error(error):
                    logger.error(error)
                    break
                }}
            .disposed(by: self.viewModelBag)
    }
    
    // MARK: deprecated
    private func fetchUserList() {
        let serial = ApiConfig.getApiToken()
        
        // 轉成物件
        apiManager.request(AiStoreApi.SendSerialNo(serial))
            .delay(3, scheduler: MainScheduler.instance)
            .flatMap { _ in
                return self.apiManager.requestReturnDecodable(AiStoreApi.ReceiveCurrentPerson(serial)) }
            .do(onSuccess: { _ in
                logger.debug("do onSuccess")
            }, onError: { _ in
                logger.debug("do onError")
            }, onSubscribe: {
                logger.debug("do subscribe")
                self.isFetching = true
                self.progressingPublish.onNext(true)
            }, onSubscribed: {
                logger.debug("do subscribed")
            }, onDispose: {
                logger.debug("do onDispose")
                self.isFetching = false
                self.progressingPublish.onNext(false)
            })
            .subscribe { result in
                switch result {
                case let .success(response):
                    if (response.count > 0) {
                        self.userList = response[0].userArray
                    }
                    break
                case let .error(error):
                    logger.error(error)
                    break
                }}
            .disposed(by: self.viewModelBag)
        
        /*
        // 轉成字串
        apiManager.request(AiStoreApi.SendSerialNo(serial))
            .flatMap { _ in
                return self.apiManager.request(AiStoreApi.ReceiveCurrentPerson(serial))}
            .mapString()
            .subscribe { result in
                switch result {
                case let .success(response):
                    logger.verbose(response)
                    break
                case let .error(error):
                    logger.error(error)
                    break
                }}
         .disposed(by: dispose)
        */
    }
}

extension LoginViewModel: ObserableTransform {
    
    struct Input {
        let viewWillAppear: Driver<Void>
        let viewDidDisappear: Driver<Void>
        let account: Driver<String>
        let password: Driver<String>
        let tapLogin: Observable<Void>
    }
    
    struct Output {
        let appearAction: Driver<Void>
        let disappearAction: Driver<Void>
        let loginEnable: Driver<Bool>
        let loginAction: Observable<(LoginResult, String)>
    }
    
    func transform(input: Input) -> Output {
        let appearAction = input.viewWillAppear.do(onNext: {
            //self.fetchUserList()
        })
        
        let disappearAction = input.viewDidDisappear.do(onNext: {
            self.userList = nil
            self.clearDisposeBag()
        })
        
        let accountAndPassword = Driver.combineLatest(input.account, input.password)
        
        let loginEnable = accountAndPassword
            .asDriver()
            .map({ (account, password) in
                return account.count > 0 && password.count > 0
            })
        
        let loginAction = input.tapLogin
            .withLatestFrom(accountAndPassword)
            // latest: false 觸發後, 下次觸發等到間隔後.
            .throttle(2, latest: false, scheduler: MainScheduler.instance)
            // 多行
            //.flatMap({ (arg) -> Single<(LoginResult, String)> in
                //let (account, password) = arg
            .flatMap({ account, password in
                return self.tryLogin(account, password, usedCached: false)
                    .do(onSubscribe: {
                        self.isFetching = true
                        self.progressingPublish.onNext(true)
                    }, onDispose: {
                        self.isFetching = false
                        self.progressingPublish.onNext(false)
                    })
            })
        
        return Output(appearAction: appearAction, disappearAction: disappearAction, loginEnable: loginEnable, loginAction: loginAction)
    }
}
