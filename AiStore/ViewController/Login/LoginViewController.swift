//
//  ViewController.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/2/7.
//  Copyright © 2020 TCIT. All rights reserved.
//

import UIKit
import MBProgressHUD
import RxSwift

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var iconMarginTop: NSLayoutConstraint!
    
    @IBOutlet weak var textAccount: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var labelVersion: UILabel!
    
    private var viewModel = LoginViewModel()
    
    private var currentEditingField: UITextField? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        bindViewModel()
    }
    
    private func setupUI() {
        textAccount.delegate = self
        textPassword.delegate = self
        
        textAccount.returnKeyType = .next
        textPassword.returnKeyType = .done
        
        labelVersion.text = "Version: " + Bundle.main.version
        
        let (initAccount, initPassword) = AccountManager.shared.fetchUserLogin()
        textAccount.text = initAccount
        //textPassword.text = initPassword
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.singleTap(recognizer:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func singleTap(recognizer:UITapGestureRecognizer){
        dismissKeyboard()
    }
    
    private func dismissKeyboard() {
        if let field = currentEditingField {
            field.resignFirstResponder()
        }
    }
    
    private func bindViewModel() {
        
        btnRegister.rx.tap
            .throttle(2, scheduler: MainScheduler.instance)
            .bind {
                self.dismissKeyboard()
                self.presentRegisterView(isPresent: false)
        }
        .disposed(by: self.disposeBag)
        
        viewModel.progressingPublish
            .subscribe({ event in
                if (true == event.element) {
                    self.dismissKeyboard()
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            })
            .disposed(by: self.disposeBag)
        
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let viewDidDisappear = rx.sentMessage(#selector(UIViewController.viewDidDisappear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let output: LoginViewModel.Output = viewModel.transform(input: LoginViewModel.Input(
            viewWillAppear: viewWillAppear,
            viewDidDisappear: viewDidDisappear,
            account: textAccount.rx.text.orEmpty.asDriver(),
            password: textPassword.rx.text.orEmpty.asDriver(),
            tapLogin: btnLogin.rx.tap.mapToVoid()))
        
        output.appearAction
            .drive()
            .disposed(by: self.disposeBag)
        
        output.disappearAction
            .drive()
            .disposed(by: self.disposeBag)
        
        output.loginEnable
            .drive(btnLogin.rx.isEnabled)
            .disposed(by: self.disposeBag)
        
        output.loginAction
            .subscribe({ event in
                if let (result, message) = event.element {
                    var showMessage = false
                    
                    logger.debug("Login result: \(result), Message: \(message)")
                    switch result {
                    case .Successed:
                        self.presentMenuView(isPresent: false)
                        break
                    case .Loading:
                        break
                    case .NoData:
                        showMessage = true
                        break
                    case .NoMatch:
                        showMessage = true
                        break
                    }
                    
                    if (showMessage) {
                        let alertVC = AlertControllerFactory.createConfirmAlertController(title: message)
                        self.present(alertVC, animated: true, completion: nil)
                    }
                }
            })
            .disposed(by: self.disposeBag)
    }
    
    private func presentRegisterView(isPresent: Bool) {
        if let nextVC = storyboard?.instantiateViewController(withIdentifier: "Register") as? RegisterViewController {
            nextVC.isRegister = true
            presentViewController(isPresent: isPresent, nextVC: nextVC)
        }
    }
    
    private func presentMenuView(isPresent: Bool) {
        if let nextVC = storyboard?.instantiateViewController(withIdentifier: "Menu") as? MenuViewController {
            presentViewController(isPresent: isPresent, nextVC: nextVC)
        }
    }
    
    override func keyboardWillHide(notification: Notification) {
        iconMarginTop.constant = 30
    }
    
    override func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            iconMarginTop.constant = 30 - keyboardSize.height / 2
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        switch textField {
        case textAccount:
            textPassword.becomeFirstResponder()
        default:
            textField.endEditing(true)
            break
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentEditingField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        currentEditingField = nil
    }
}
