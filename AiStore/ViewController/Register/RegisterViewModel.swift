//
//  RegisterViewModel.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/2/10.
//  Copyright © 2020 TCIT. All rights reserved.
//

import Foundation
import Moya
import RxMoya
import RxSwift
import RxCocoa
import UIKit

final class RegisterViewModel: BaseViewModel {
    fileprivate(set) var nameReady = false
    fileprivate(set) var numberReady = false
    fileprivate(set) var passwordReady = false
    fileprivate(set) var phoneReady = false
    fileprivate(set) var heightReady = false
    fileprivate(set) var weightReady = false
    fileprivate(set) var registerReady = false
    
    var isRegister = true
    
    let contentOffsetPublish: PublishSubject<Int> = PublishSubject.init()
    
    private let apiManager = ApiManager.shared
    
    var isFetching = false
    
    private var imageBase64 = ""
    
    func updatedImage(image: Image) {
        imageBase64 = convertImageToBase64String(image: image)
    }
    
    private func updatedProfile(name: String, account: String, phone: String, password: String, tall: String, weight: String) -> Single<ApiResult> {
        let userInfo = AccountManager.shared.fetchUserInfomation()
        let body = ApiRequestList.Profile(action: "send",
                                          function: "updateProfile",
                                          account: ApiConfig.ACCOUNT,
                                          keeperSerialNo: userInfo?.keeperSerialNo,
                                          name: name,
                                          schoolNo: account,
                                          contactNo: phone,
                                          password: password,
                                          picString: self.imageBase64.notEmpty ? self.imageBase64 : nil,
                                          tall: tall,
                                          weight: weight)
        return self.apiManager.sendProfile(body: body)
            .flatMap({ _ in
                let nextBody = ApiRequestList.RegisterUserInfo(cardId: body.keeperSerialNo, accountId: body.account, name: body.name, token: ApiConfig.getApiToken(), height: Int(body.tall), weight: Int(body.weight))
                return self.apiManager.registerUserInfo(body: nextBody)
                    .map({ response in
                        switch (response.statusCode) {
                            case 200:
                                return ApiResult.Successed
                            default:
                                return ApiResult.Failure("建檔失敗")
                        }
                    })
            })
    }
    
    private func tryRegister(name: String, account: String, phone: String, password: String, tall: String, weight: String) -> Single<ApiResult> {
        if (imageBase64.isEmpty) {
            return Single.just(ApiResult.Failure(""))
        }
        
        return apiManager.fetchUsers(serial: ApiConfig.getApiToken())
            .flatMap({ userArray -> Single<ApiResult> in
                if let userArray = userArray {
                    if !self.isMember(userArray, account) {
                        let serialNo = ApiConfig.getApiToken()
                        let body = ApiRequestList.Profile(action: "send",
                                                          function: "createProfile",
                                                          account: ApiConfig.ACCOUNT,
                                                          keeperSerialNo: serialNo,
                                                          name: name,
                                                          schoolNo: account,
                                                          contactNo: phone,
                                                          password: password,
                                                          picString: self.imageBase64,
                                                          tall: tall,
                                                          weight: weight)
                        return self.apiManager.sendProfile(body: body)
                            .delay(1.5, scheduler: MainScheduler.instance)
                            .flatMap({ _ -> Single<[UserData]?> in
                                return self.apiManager.fetchUsers(serial: ApiConfig.getApiToken())
                            })
                            .delay(1, scheduler: MainScheduler.instance)
                            .flatMap({ userArray -> Single<ApiResult> in
//                                if let userArray = userArray {
//                                    if self.isMember(userArray, account) {
                                let nextBody = ApiRequestList.RegisterUserInfo(cardId: body.keeperSerialNo,
                                                                               accountId: body.account,
                                                                               name: body.name,
                                                                               token: ApiConfig.getApiToken(),
                                                                               height: Int(body.tall),
                                                                               weight: Int(body.weight))
                                return self.apiManager.registerUserInfo(body: nextBody)
                                    .map({ response in
                                        switch (response.statusCode) {
                                        case 200:
                                            return ApiResult.Successed
                                        default:
                                            return ApiResult.Failure("建檔失敗")
                                        }
                                    })
//                                    }
//                                }
//                                return Single.just(ApiResult.Failure("建檔失敗"))
                            })
                    } else {
                        return Single.just(ApiResult.Failure("註冊資訊重複"))
                    }
                }
                
                return Single.just(ApiResult.NoData)
            })
    }
    
    private func isMember(_ userList: [UserData], _ account: String) -> Bool {
        for i in 0..<userList.count {
            let user = userList[i]
            if user.schoolNo == account {
                return true
            }
        }
        return false
    }
}

extension RegisterViewModel: ObserableTransform {
    
    struct Input {
        let name: Driver<String>
        let number: Driver<String>
        let password: Driver<String>
        let phone: Driver<String>
        let height: Driver<String>
        let weight: Driver<String>
        let tapConfirm: Observable<Void>
        let tapTakePhoto: Driver<Void>
    }
    
    struct Output {
        let bmi: Driver<String>
        let confirmAction: Observable<ApiResult>
        let takePhotoAction: Driver<Void>
    }
    
    func transform(input: Input) -> Output {
        
        let combineBmi = Driver.combineLatest(input.height, input.weight)
        
        let bmi = combineBmi
            .map { (heightString, weightString) -> String in
                
                var result: Float? = nil
                
                var h: Float = 0, w: Float = 0
                
                if let hFloat = heightString.floatValue {
                    h = hFloat
                    self.heightReady = h > 60 && h < 220
                } else {
                    self.heightReady = false
                }
                
                if let wFloat = weightString.floatValue {
                    w = wFloat
                    self.weightReady = w > 30 && w < 130
                } else {
                    self.weightReady = false
                }
                
                if (self.heightReady && self.weightReady) {
                    let hM = h / 100
                    result = w / (hM * hM)
                }
                
                if let result = result {
                    return String(format: "%.1f", result)
                } else {
                    return ""
                }
        }
        
        let comibeInput = Driver.combineLatest(input.name, input.number, input.password, input.phone, bmi)
        let combineAll = Driver.combineLatest(comibeInput, combineBmi)
        
        let confirmAction = input.tapConfirm
            .withLatestFrom(combineAll)
            .throttle(2, latest: false, scheduler: MainScheduler.instance)
            .flatMap({(arg) -> Single<ApiResult> in
                let ((name, number, password, phone, _), (height, weight)) = arg
                if (self.isRegister) {
                    if (self.imageBase64.isEmpty) {
                        return Single.just(ApiResult.Failure("請拍攝照片"))
                    }
                    if (name.isEmpty || phone.isEmpty || password.isEmpty || phone.isEmpty || height.isEmpty || weight.isEmpty) {
                        return Single.just(ApiResult.Failure("請填寫資料"))
                    }
                    
                    return self.tryRegister(name: name, account: number, phone: phone, password: password, tall: height, weight: weight)
                        .do(onSubscribe: {
                            self.isFetching = true
                            self.progressingPublish.onNext(true)
                        }, onDispose: {
                            self.isFetching = false
                            self.progressingPublish.onNext(false)
                        })
                } else {
                    return self.updatedProfile(name: name, account: number, phone: phone, password: password, tall: height, weight: weight)
                        .do(onSubscribe: {
                            self.isFetching = true
                            self.progressingPublish.onNext(true)
                        }, onDispose: {
                            self.isFetching = false
                            self.progressingPublish.onNext(false)
                        })
                }
            })
        /*
        .flatMap({(arg) -> Observable<ApiResult> in
            let ((name, number, password, phone, bmi), (height, weight)) = arg
            logger.verbose("\(name), \(number), \(password), \(phone), \(bmi), \(height), \(weight)")
            return Observable.just(ApiResult.Failure)
        })
         */
        
        return Output(bmi: bmi, confirmAction: confirmAction, takePhotoAction: input.tapTakePhoto)
    }
}

extension RegisterViewModel {
    private func convertImageToBase64String (image: UIImage) -> String {
        
        var newImage: UIImage? = nil
        newImage = image.rotate(radians: 0.0)
        
        if (newImage == nil) {
            newImage = image
        }
        
        var quality = CGFloat(1)
        var data = newImage!.jpegData(compressionQuality: quality)!
        while (data.count > 200 * 1024)  {
            quality *= 0.9
            data = newImage!.jpegData(compressionQuality: quality)!
        }
        
        return data.base64EncodedString()
    }
    
    
    private func convertBase64StringToImage (base64String: String) -> UIImage? {
        if let imageData = Data.init(base64Encoded: base64String, options: .init(rawValue: 0)) {
            if let image = UIImage(data: imageData) {
                return image
            }
        }
        
        return nil
    }
}
