//
//  RegisterViewController.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/2/10.
//  Copyright © 2020 TCIT. All rights reserved.
//

import UIKit
import RxSwift
import MBProgressHUD

class RegisterViewController: BaseViewController {
    
    enum TextFieldType {
        case Name
        case Number
        case Password
        case Phone
        case Height
        case Weight
    }
    
    @IBOutlet weak var contentMarginBottom: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var textName: UITextField!
    @IBOutlet weak var textNumber: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var textPhone: UITextField!
    @IBOutlet weak var textHeight: UITextField!
    @IBOutlet weak var textWeight: UITextField!
    @IBOutlet weak var textBMI: UITextField!
    
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSwitchCamera: UIButton!
    @IBOutlet weak var btnTakePhoto: UIButton!
    
    @IBOutlet weak var cameraControlView: UIView!
    
    private var currentEditingField: UITextField? = nil
    
    private var viewModel = RegisterViewModel()
    
    private var cameraController = CameraController()
    
    var isRegister = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hiddenNavigationBar = false
        
        viewModel.isRegister = isRegister
        
        if (isRegister) {
            title = "註冊"
            btnConfirm.setTitle("確定註冊", for: .normal)
            btnCancel.setTitle("取消註冊", for: .normal)
            
            // 隱藏返回
            self.navigationItem.hidesBackButton = true
        } else {
            textNumber.isEnabled = false
            textNumber.textColor = UIColor.gray
            
            title = "修改個人資料"
            btnConfirm.setTitle("儲存修改", for: .normal)
            btnCancel.setTitle("取消修改", for: .normal)
            
            self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "BackBtnIcon")?.withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(backButtonAction))
            self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "HomeIcon")?.withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(backButtonAction))
            
            if let initData = AccountManager.shared.fetchUserInfomation() {
                textName.text = initData.name
                textNumber.text = initData.schoolNo
                textPassword.text = initData.password
                textPhone.text = initData.contactNo
                textHeight.text = initData.tall
                textWeight.text = initData.weight
            }
        }
        
        setupUI()
        
        bindViewModel()
        
        setupCamera()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.singleTap(recognizer:)))
        self.scrollView.addGestureRecognizer(tapGesture)
    }
    
    @objc func singleTap(recognizer:UITapGestureRecognizer){
        dismissKeyboard()
    }
    
    private func dismissKeyboard() {
        if let field = currentEditingField {
            field.resignFirstResponder()
        }
    }
    
    private func setupUI() {
        setupKeyboard(textName)
        setupKeyboard(textNumber)
        setupKeyboard(textPassword)
        setupKeyboard(textPhone)
        setupKeyboard(textHeight)
        setupKeyboard(textWeight)
    }
    
    private func bindViewModel() {
        viewModel.progressingPublish
            .subscribe({ event in
                if (true == event.element) {
                    self.dismissKeyboard()
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            })
            .disposed(by: self.disposeBag)
        
        btnCancel.rx.tap
            .throttle(2, scheduler: MainScheduler.instance)
            .bind {
                self.dismissKeyboard()
                self.navigationController?.popViewController(animated: true)
        }
        .disposed(by: self.disposeBag)
        
        btnSwitchCamera.rx.tap
            .throttle(2, scheduler: MainScheduler.instance)
            .bind {
                try? self.cameraController.switchCameras()
        }
        .disposed(by: self.disposeBag)
        
        viewModel.contentOffsetPublish.subscribe(onNext:{ offset in
            UIView.animate(withDuration: 0.3, animations: {
                self.scrollView.contentOffset = CGPoint(x: 0, y: offset)
            })
        }).disposed(by: self.disposeBag)
        
        let output: RegisterViewModel.Output = viewModel.transform(input: RegisterViewModel.Input(
            name: textName.rx.text.orEmpty.asDriver(),
            number: textNumber.rx.text.orEmpty.asDriver(),
            password: textPassword.rx.text.orEmpty.asDriver(),
            phone: textPhone.rx.text.orEmpty.asDriver(),
            height: textHeight.rx.text.orEmpty.asDriver(),
            weight: textWeight.rx.text.orEmpty.asDriver(),
            tapConfirm: btnConfirm.rx.tap.mapToVoid(),
            tapTakePhoto: btnTakePhoto.rx.tap.asDriver()))
        
        output.bmi
            .drive(textBMI.rx.text)
            .disposed(by: self.disposeBag)
        
        var isCameraPause = false
        output.takePhotoAction
            .asObservable()
            .subscribe({ _ in
                isCameraPause = !isCameraPause
                if (isCameraPause) {
                    self.cameraController.captureImage { (image, error) in
                        guard let image = image else {
                            if error != nil {
                                logger.error(error)
                            } else {
                                logger.error("Image capture error")
                            }
                            return
                        }
                        
                        self.viewModel.updatedImage(image: image)
                        
                        self.cameraController.pause()
                        self.btnSwitchCamera.isHidden = true
                    }
                } else {
                    self.cameraController.resume()
                    self.btnSwitchCamera.isHidden = false
                }
            })
            .disposed(by: self.disposeBag)
        
        output.confirmAction
            .subscribe({ event in
                var message: String? = nil
                
                let result = event.element
                switch (result) {
                case .none:
                    message = "建檔失敗"
                    break
                case .Successed:
                    self.popToLoginViewController()
                    break
                case .Loading:
                    break
                case .NoData:
                    message = "建檔失敗"
                    break
                case .Failure(let msg):
                    message = msg
                    break
                }

                if let message = message {
                    let alertVC = AlertControllerFactory.createConfirmAlertController(title: message)
                    self.present(alertVC, animated: true, completion: nil)
                }
            })
            .disposed(by: self.disposeBag)
    }
    
    private func popToLoginViewController() {
        if let vc = self.navigationController?.viewControllers.filter({$0 is LoginViewController}).first {
            self.navigationController?.popToViewController(vc, animated: true)
        }
    }
    
    private func setupKeyboard(_ textFiled: UITextField) {
        textFiled.returnKeyType = .next
        textFiled.delegate = self
    }
    
    private func setupCamera() {
        cameraController.prepare(completionHandler: { (error) in
            if let error = error {
                logger.error(error)
            } else {
                let w = CGFloat(150), h = CGFloat(200), x = (self.view.frame.width - w) / 2
                try? self.cameraController.displayPreview(on: self.cameraControlView, frame: CGRect(x: x, y: 0, width: w, height: h))
            }
        })
    }
    
    override func keyboardWillHide(notification: Notification) {
        contentMarginBottom.constant = 20
    }
    
    override func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            contentMarginBottom.constant = keyboardSize.height
        }
    }
    
    func textFieldConvertType(_ textField: UITextField) -> TextFieldType? {
        let type: TextFieldType?
        switch textField {
        case textName:
            type = .Name
        case textNumber:
            type = .Number
        case textPassword:
            type = .Password
        case textPhone:
            type = .Phone
        case textHeight:
            type = .Height
        case textWeight:
            type = .Weight
        default:
            type = nil
        }
        
        return type
    }
}

extension RegisterViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentEditingField = textField
        
        if let type = textFieldConvertType(textField) {
            let level: Int
            switch type {
            case .Name:
                level = 0
            case .Number:
                level = 1
            case .Password:
                level = 2
            case .Phone:
                level = 3
            case .Height:
                level = 4
            case .Weight:
                level = 5
            }
            
            viewModel.contentOffsetPublish.onNext(60 * level)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        currentEditingField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (viewModel.registerReady) {
            textField.endEditing(true)
        } else {
            if let type = textFieldConvertType(textField) {
                let textFieldReady: Bool
                switch type {
                case .Name:
                    textFieldReady = viewModel.nameReady
                case .Number:
                    textFieldReady = viewModel.numberReady
                case .Password:
                    textFieldReady = viewModel.passwordReady
                case .Phone:
                    textFieldReady = viewModel.phoneReady
                case .Height:
                    textFieldReady = viewModel.heightReady
                case .Weight:
                    textFieldReady = viewModel.weightReady
                }
                
                if !textFieldReady {
                    return false
                }
            }
            
            let nextTextField: UITextField?
            if !viewModel.nameReady {
                nextTextField = textName
            } else if !viewModel.numberReady {
                nextTextField = textNumber
            } else if !viewModel.passwordReady {
                nextTextField = textPassword
            } else if !viewModel.phoneReady {
                nextTextField = textPhone
            } else if !viewModel.heightReady {
                nextTextField = textHeight
            } else if !viewModel.weightReady {
                nextTextField = textWeight
            } else {
                nextTextField = nil
            }
            
            if let nextTextField = nextTextField {
                nextTextField.becomeFirstResponder()
            } else {
                return false
            }
        }
        
        return true
    }
}
