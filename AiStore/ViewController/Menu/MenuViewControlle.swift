//
//  MenuViewControlle.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/3/31.
//  Copyright © 2020 TCIT. All rights reserved.
//

import UIKit
import RxSwift

class MenuViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hiddenNavigationBar = false
        
        // 隱藏返回
        self.navigationItem.hidesBackButton = true
    }
}

extension MenuViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        if let _cell = cell as? MenuCell {
            let (title, icon) = getTableViewCellData(index: indexPath.row)
            _cell.labelTitle.text = title
            _cell.ivIcon.image = icon
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        logger.verbose("\(indexPath.row)")
        
        if (indexPath.row == 0) {
            if let nextVC = storyboard?.instantiateViewController(withIdentifier: "Record") as? RecordViewController {
                nextVC.title = "我的消費記錄"
                navigationController?.pushViewController(nextVC, animated: true)
            }
        } else if (indexPath.row == 1) {
            if let nextVC = storyboard?.instantiateViewController(withIdentifier: "Register") as? RegisterViewController {
                nextVC.isRegister = false
                navigationController?.pushViewController(nextVC, animated: true)
            }
        } else if (indexPath.row == 2) {
            if let nextVC = storyboard?.instantiateViewController(withIdentifier: "WebView") as? WebViewController {
                navigationController?.pushViewController(nextVC, animated: true)
            }
        }
    }
    
    func getTableViewCellData(index: Int) -> (String, UIImage?) {
        var resultText: String
        var resultIcon: UIImage?
        
        switch (index) {
        case 0:
            resultText = "我的消費記錄"
            resultIcon = UIImage.init(named: "MenuMoney")
        case 1:
            resultText = "修改個人資料"
            resultIcon = UIImage.init(named: "AccountIcon")
        case 2:
            resultText = "永春3R學習平台"
            resultIcon = UIImage.init(named: "MenuPlatform")
        default:
            resultText = ""
            resultIcon = nil
        }
        
        return (resultText, resultIcon)
    }
}
