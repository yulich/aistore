//
//  WebViewController.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/4/30.
//  Copyright © 2020 TCIT. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: BaseViewController {
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hiddenNavigationBar = false
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "BackBtnIcon")?.withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(backButtonAction))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "HomeIcon")?.withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(closeButtonAction))

        webView.load(URLRequest(url: URL(string: "https://ycsh.vrfantasy.gallery/")!))
    }
    
    override func backButtonAction() {
        if (webView.canGoBack) {
            webView.goBack()
        } else {
            super.backButtonAction()
        }
    }
    
    @objc func closeButtonAction() {
        super.backButtonAction()
    }
}
