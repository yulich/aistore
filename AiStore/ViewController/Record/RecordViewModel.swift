//
//  RecordViewModel.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/4/30.
//  Copyright © 2020 TCIT. All rights reserved.
//

import Foundation
import Moya
import RxMoya
import RxSwift
import RxCocoa

final class RecordViewModel: BaseViewModel {
    
    private let apiManager = ApiManager.shared
    
    private let accountManager = AccountManager.shared
    
    func fetchBuyList() -> Single<BuyListModel> {
        if let userData = accountManager.fetchUserInfomation() {
            if let name = userData.name, let account = userData.schoolNo {
                return apiManager.fetchBuyList(name, account)
            }
        }
        
        return Single.just([])
    }
}

extension RecordViewModel: ObserableTransform {
    
    struct Input {
        let viewWillAppear: Observable<Void>
        let pullTableView: Observable<Void>
        let viewDidDisappear: Driver<Void>
    }
    
    struct Output {
        let disappearAction: Driver<Void>
        let responeAction: Driver<BuyListModel>
    }
    
    func transform(input: Input) -> Output {
        let mergeObservable = Observable.merge(input.viewWillAppear, input.pullTableView)
        
        let responeAction = mergeObservable
            .throttle(1, latest: false, scheduler: SerialDispatchQueueScheduler(qos: .background))
            .flatMap {
                return self.fetchBuyList()
                    .do(onSuccess: { _ in
                        logger.verbose("Load onSuccess")
                        self.progressingPublish.onNext(false)
                    }, onError: { _ in
                        logger.verbose("Load onError")
                        self.progressingPublish.onNext(false)
                    }, onSubscribe: {
                        logger.verbose("Loading")
                        self.progressingPublish.onNext(true)
                    })
                    .catchError { error in
                        logger.error(error)
                        return Single.just([])
                }}
            .asDriver(onErrorJustReturn: []) // driver觀察時, 會轉換成main thread.
        
        let disappearAction = input.viewDidDisappear.do(onNext: {
            self.clearDisposeBag()
        })
        
        return Output(disappearAction: disappearAction, responeAction: responeAction)
    }
}
