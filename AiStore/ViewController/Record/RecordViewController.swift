//
//  RecordViewController.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/4/30.
//  Copyright © 2020 TCIT. All rights reserved.
//

import UIKit
import RxSwift
import MBProgressHUD

class RecordViewController: BaseViewController {
    
    private var viewModel = RecordViewModel()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hiddenNavigationBar = false
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "BackBtnIcon")?.withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(backButtonAction))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "HomeIcon")?.withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(backButtonAction))
        
        tableView.refreshControl = UIRefreshControl()
        
        bindViewModel()
    }
    
    private func bindViewModel() {
        viewModel.progressingPublish
            .subscribe({ event in
                if (true == event.element) {
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                self.tableView.refreshControl?.endRefreshing()
            })
            .disposed(by: disposeBag)
        
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
        
        let viewDidDisappear = rx.sentMessage(#selector(UIViewController.viewDidDisappear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let pull = tableView.refreshControl!.rx
            .controlEvent(.valueChanged).mapToVoid()
        
        let output: RecordViewModel.Output = viewModel.transform(input: RecordViewModel.Input(
            viewWillAppear: viewWillAppear,
            pullTableView: pull,
            viewDidDisappear: viewDidDisappear))
        
        output.responeAction.drive(tableView.rx.items(cellIdentifier: "Cell", cellType: RecordCell.self)) { (index, data, cell) in
            cell.bind(model: data)
            cell.delegate = self
        }
        .disposed(by: self.disposeBag)
        
        output.disappearAction
            .drive()
            .disposed(by: self.disposeBag)
    }
}

extension RecordViewController: RecordCellDelegate {
    func onClickedDetail(data: BuyListElement) {
        if let nextVC = storyboard?.instantiateViewController(withIdentifier: "Detail") as? RecordDetailViewController {
            nextVC.bind(title: self.title, time: data.time, data: data.receipt, totalPrice: data.totalPrice)
            navigationController?.pushViewController(nextVC, animated: true)
        }
    }
}
