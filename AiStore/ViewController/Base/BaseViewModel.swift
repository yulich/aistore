//
//  BaseViewModel.swift
//  CleanArchitectureRxMoya
//
//  Created by Jeff Yu on 2020/2/5.
//  Copyright © 2020 Jeff. All rights reserved.
//

import Moya
import RxMoya
import RxSwift
import RxCocoa

open class BaseViewModel {
    
    deinit {
        logger.verbose(self.theClassName)
    }
    
    fileprivate var _viewModelBag: DisposeBag?
    
    var viewModelBag: DisposeBag {
        get {
            if (_viewModelBag == nil) {
                _viewModelBag = DisposeBag()
            }
            
            return _viewModelBag!
        }
    }
    
    // Remember call when viewDidDisappear
    internal func clearDisposeBag() {
        _viewModelBag = nil
    }
    
    let progressingPublish: PublishSubject<Bool> = PublishSubject.init()
}
