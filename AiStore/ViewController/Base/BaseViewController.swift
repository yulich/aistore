//
//  BaseViewController.swift
//  CleanArchitectureRxMoya
//
//  Created by Jeff Yu on 2020/2/5.
//  Copyright © 2020 Jeff. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

open class BaseViewController: UIViewController {
    
    deinit {
        logger.verbose(self.theClassName)
    }
    
    fileprivate var _disposeBag: DisposeBag?
    
    var disposeBag: DisposeBag {
        get {
            if (_disposeBag == nil) {
                _disposeBag = DisposeBag()
            }
            
            return _disposeBag!
        }
    }
    
    func clearDisposeBag() {
        _disposeBag = nil
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if (self.navigationController == nil) {
            NotificationCenter.default.removeObserver(self)
            
            logger.verbose("View release: \(self.theClassName)")
            clearDisposeBag()
        } else {
            logger.verbose("View keep: \(self.theClassName)")
        }
    }
    
    var hiddenNavigationBar = true
    
    open override func viewDidLoad() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(BaseViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BaseViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = hiddenNavigationBar
    }
    
    @objc func keyboardWillShow(notification: Notification) { }
    
    @objc func keyboardWillHide(notification: Notification) { }
    
    func presentViewController(isPresent: Bool, nextVC: UIViewController) {
        if isPresent {
            // iOS13不會觸發"disappear"
            self.present(nextVC, animated: true, completion: nil)
        } else {
            self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }
    
    @objc internal func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
}
