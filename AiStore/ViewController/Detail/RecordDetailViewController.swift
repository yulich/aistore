//
//  RecordDetailViewController.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/5/5.
//  Copyright © 2020 TCIT. All rights reserved.
//

import UIKit

class RecordDetailViewController: BaseViewController {
    
    @IBOutlet var labelDate: UILabel!
    @IBOutlet var labelTotal: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private var dateString: String? = nil
    private var dataSrc: [Receipt]? = nil
    private var totalPrice = 0
    
    func bind(title: String?, time: String?, data: [Receipt]?, totalPrice: Int) {
        self.title = title
            
        if let time = time {
            let formatterToData = DateFormatter()
            formatterToData.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            if let date = formatterToData.date(from: time.replacingOccurrences(of: "T", with: " ", options: NSString.CompareOptions.literal, range: nil)) {
                let formatterToString = DateFormatter()
                formatterToString.dateFormat = "yyyy/MM/dd"
                self.dateString = formatterToString.string(from: date)
            }
        }
        
        self.dataSrc = data
        
        self.totalPrice = totalPrice
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hiddenNavigationBar = false
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "BackBtnIcon")?.withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(backButtonAction))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "HomeIcon")?.withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(backToMenuViewController))
        
        labelDate.text = dateString
        labelTotal.text = String(totalPrice)
    }
    
    @objc internal func backToMenuViewController() {
        if let vc = self.navigationController?.viewControllers.filter({$0 is MenuViewController}).first {
            self.navigationController?.popToViewController(vc, animated: true)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension RecordDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = dataSrc?.count {
            return count
        } else {
            return 0
        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        if let data = dataSrc?[indexPath.row] {
            if let cell = cell as? RecordDetailCell {
                cell.bind(model: data)
            }
        }
        return cell
    }
}
