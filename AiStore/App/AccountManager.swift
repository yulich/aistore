//
//  AccountManager.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/4/30.
//  Copyright © 2020 TCIT. All rights reserved.
//

import Foundation

class AccountManager {
    
    static let shared = AccountManager()
    
    private let userDefaults: UserDefaults
    
    private var _userData: UserData? = nil
    
    init() {
        userDefaults = UserDefaults.standard
    }
    
    func storedUserLogin(_ account: String, _ password: String) {
        if (!account.isEmpty && !password.isEmpty) {
            userDefaults.set(account, forKey: "account")
            userDefaults.set(password, forKey: "password")
        }
    }
    
    func fetchUserLogin() -> (String?, String?) {
        let account = userDefaults.string(forKey: "account")
        let password = userDefaults.string(forKey: "password")
        return (account, password)
    }
    
    func storedUserInfomation(_ userData: UserData) {
        _userData = userData
    }
    
    func fetchUserInfomation() -> UserData? {
        return _userData
    }
}
