//
//  Bundle+Ext.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/3/4.
//  Copyright © 2020 TCIT. All rights reserved.
//

import Foundation

extension Bundle {
    var version: String {
        return (self.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String)
    }
}
