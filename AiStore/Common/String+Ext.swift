//
//  String+Ext.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/2/26.
//  Copyright © 2020 TCIT. All rights reserved.
//

import Foundation

extension String {
    var floatValue: Float? {
        return Float(self)
    }
    
    var notEmpty: Bool {
        return !self.isEmpty
    }
}
