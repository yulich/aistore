//
//  ApiManager+AiStore.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/3/26.
//  Copyright © 2020 TCIT. All rights reserved.
//

import Foundation
import Moya
import RxSwift

extension ApiManager {
    func fetchUsers(serial: String) -> Single<[UserData]?> {
        return self.request(AiStoreApi.SendSerialNo(serial))
            .delay(1.5, scheduler: MainScheduler.instance)
            .flatMap({ _ in
                return self.requestReturnDecodable(AiStoreApi.ReceiveCurrentPerson(serial)) })
            .map({ response in
                if response.count > 0 {
                    return response[0].userArray
                }
                
                return nil
            })
    }
    
    func sendProfile(body: ApiRequestList.Profile) -> Single<Response> {
        return self.request(AiStoreApi.SendProfile(body))
    }
    
    func fetchBuyList(_ name: String, _ account: String) -> Single<BuyListModel> {
        return self.requestReturnDecodable(AiStoreApi.GetShopInfo(name, account))
    }
    
    func registerUserInfo(body: ApiRequestList.RegisterUserInfo) -> Single<Response> {
        return self.request(AiStoreApi.RegisterProfile(body))
    }
}
