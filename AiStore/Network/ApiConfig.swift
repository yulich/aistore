//
//  ApiConfig.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/3/25.
//  Copyright © 2020 TCIT. All rights reserved.
//

import Foundation

struct ApiConfig {
    static let ACCOUNT = "AI_STORE"
    
    static func getApiToken() -> String{
        return String(Int((Date().timeIntervalSince1970)) + Int.random(in: 900...2700))
    }
}
