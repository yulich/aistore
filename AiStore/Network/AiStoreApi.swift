//
//  AiStoreApi.swift
//  CleanArchitectureRxMoya
//
//  Created by Jeff Yu on 2020/3/20.
//  Copyright © 2020 Jeff. All rights reserved.
//

import Foundation
import Moya
import RxSwift

/**
 https://qiita.com/kouheiszk/items/46e9a233d9bb227c3b1d
 
 https://medium.com/@davidlin_98861/%E8%8F%AF%E9%BA%97%E7%9A%84-network-layer-c5c664dcca47
 */

protocol AiStoreApiTargetType: DecodableResponseTargetType { }

extension AiStoreApiTargetType {
    
    var baseURL: URL {
        #if DEBUG
        return URL(string: "http://220.130.165.91:8854/AISTORE")!
        #else
        return URL(string: "http://220.130.165.91:8854/AISTORE")!
        #endif
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var sampleData: Data {
        let path = Bundle.main.path(forResource: "samples", ofType: "json")!
        return FileHandle(forReadingAtPath: path)!.readDataToEndOfFile()
    }
}

enum AiStoreApi {
    
    struct SendSerialNo: AiStoreApiTargetType {
        var path = ""
        
        let parameters: ApiRequestList.GetCurrentPerson
        
        init(_ serial: String) {
            parameters = ApiRequestList.GetCurrentPerson(action: "send", function: "getCurrentPerson", account: ApiConfig.ACCOUNT, serialNo: serial)
        }
        
        var method: Moya.Method { return .post }
        
        typealias ResponseType = NullModel
        
        var task: Task {
            return .requestJSONEncodable(parameters)
        }
    }
    
    struct ReceiveCurrentPerson: AiStoreApiTargetType {
        var path = "/api/Value/GetShopInform"
        
        let parameters: ApiRequestList.GetCurrentPerson
        
        init(_ serial: String) {
            parameters = ApiRequestList.GetCurrentPerson(action: "receive", function: "getCurrentPerson", account: ApiConfig.ACCOUNT, serialNo: serial)
        }
        
        var method: Moya.Method { return .post }
        
        typealias ResponseType = GetCurrentPerson
        
        var task: Task {
            return .requestJSONEncodable(parameters)
        }
    }
    
    struct SendProfile: AiStoreApiTargetType {
        var path = ""
        
        let parameters: ApiRequestList.Profile
        
        init(_ body: ApiRequestList.Profile) {
            parameters = body
        }
        
        var method: Moya.Method { return .post }
        
        typealias ResponseType = NullModel
        
        var task: Task {
            return .requestJSONEncodable(parameters)
        }
    }
    
    struct GetShopInfo: AiStoreApiTargetType {
        var baseURL: URL = URL(string: "http://220.130.165.91:8817")!
        
        var path = "/api/Value/GetShopInform"
        
        let parameters: ApiRequestList.GetShopInfo
        
        init(_ name: String,_ account: String) {
            parameters = ApiRequestList.GetShopInfo(name: name, account: account)
        }
        
        var method: Moya.Method { return .post }
        
        typealias ResponseType = BuyListModel
        
        var task: Task {
            return .requestJSONEncodable(parameters)
        }
    }
    
struct RegisterProfile: AiStoreApiTargetType {
        var baseURL: URL = URL(string: "http://220.130.165.91:8817")!
        
        var path = "/api/Value/Register"
        
        let parameters: ApiRequestList.RegisterUserInfo
        
        init(_ body: ApiRequestList.RegisterUserInfo) {
            parameters = body
        }
        
        var method: Moya.Method { return .post }
        
        typealias ResponseType = NullModel
        
        var task: Task {
            return .requestJSONEncodable(parameters)
        }
    }
}
