//
//  ApiRequestList.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/3/20.
//  Copyright © 2020 TCIT. All rights reserved.
//

import Foundation

class ApiRequestList {
    struct GetCurrentPerson: Encodable {
        var action: String
        var function: String
        var account: String
        var serialNo: String
        
        enum CodingKeys: String, CodingKey {
            case action = "Action"
            case function = "Function"
            case account = "Account"
            case serialNo = "SerialNo"
        }
    }
    
    struct Profile: Encodable {
        var action: String
        var function: String
        var account: String
        var keeperSerialNo: String?
        var name: String
        var parentName: String?
        var schoolNo: String
        var contactNo: String
        var password: String
        var status: Bool?
        var firstLogTime: String?
        var endLogTime: String?
        var picString: String?
        var tall: String
        var weight: String
        
        enum CodingKeys: String, CodingKey {
            case action = "Action"
            case function = "Function"
            case account = "Account"
            case keeperSerialNo = "keeperSerialNo"
            case name = "name"
            case parentName = "parentName"
            case schoolNo = "schoolNo"
            case contactNo = "contactNo"
            case password = "password"
            case status = "status"
            case firstLogTime = "firstLogTime"
            case endLogTime = "endLogTime"
            case picString = "picString"
            case tall = "tall"
            case weight = "weight"
        }
        
    }
    
    struct GetShopInfo: Encodable {
        var name: String
        var account: String
        
        enum CodingKeys: String, CodingKey {
            case name = "Name"
            case account = "Account"
        }
    }
    
    struct RegisterUserInfo: Encodable {
        var cardId: String?
        var accountId: String
        var name: String
        var token: String
        var height: Int?
        var weight: Int?
        
        enum CodingKeys: String, CodingKey {
            case cardId = "Card_Id"
            case accountId = "Account_Id"
            case name = "Name"
            case token = "Token"
            case height = "Height"
            case weight = "Weight"
        }
    }
}
