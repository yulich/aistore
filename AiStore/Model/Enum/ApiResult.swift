//
//  ApiResult.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/4/30.
//  Copyright © 2020 TCIT. All rights reserved.
//

enum ApiResult {
    case Successed
    case Failure(String)
    case Loading
    case NoData
}
