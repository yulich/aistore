//
//  GetCurrentPerson.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/3/20.
//  Copyright © 2020 TCIT. All rights reserved.
//
//  This file was generated from JSON Schema using quicktype, do not modify it directly.
//  To parse the JSON, add this file to your project and do:
//
//  let getCurrentPerson = try? newJSONDecoder().decode(GetCurrentPerson.self, from: jsonData)

import Foundation

// MARK: - GetCurrentPersonElement
struct GetCurrentPersonElement: Codable {
    let account, serialNo, action: String
    let userArray: [UserData]

    enum CodingKeys: String, CodingKey {
        case account = "Account"
        case serialNo = "SerialNo"
        case action = "Action"
        case userArray
    }
}

// MARK: - UserData
struct UserData: Codable {
    let keeperInfo: String?
    let parentName, password, picString, keeperSerialNo: String?
    let firstLogTime, name, endLogTime, schoolNo: String?
    let contactNo: String?
    let status: Bool?
    let weight, tall: String?
}

typealias GetCurrentPerson = [GetCurrentPersonElement]

