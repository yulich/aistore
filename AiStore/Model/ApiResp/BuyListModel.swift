//
//  BuyListItem.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/5/4.
//  Copyright © 2020 TCIT. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let buyList = try? newJSONDecoder().decode(BuyList.self, from: jsonData)

import Foundation

// MARK: - BuyListElement
struct BuyListElement: Codable {
    let name: String?
    let account: String?
    let totalPrice: Int
    let totalCalories: Int
    let time: String?
    let receipt: [Receipt]?

    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case account = "Account"
        case totalPrice = "Total_Price"
        case totalCalories = "Total_Calories"
        case time = "Time"
        case receipt = "Receipt"
    }
}

// MARK: - Receipt
struct Receipt: Codable {
    let productName: String?
    let unitPrice: Int
    let calories: Int
    let quantity: Int

    enum CodingKeys: String, CodingKey {
        case productName = "Product_Name"
        case unitPrice = "Unit_Price"
        case calories = "Calories"
        case quantity = "Quantity"
    }
}

typealias BuyListModel = [BuyListElement]
