//
//  AlertControllerFactory.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/5/6.
//  Copyright © 2020 TCIT. All rights reserved.
//

import UIKit

class AlertControllerFactory {
    
    /// 確認提示視窗
    public static func createConfirmAlertController(title: String, okAction: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        
        // 建立提示
        let alertController = UIAlertController(
            title: title,
            message: nil,
            preferredStyle: .alert)
        
        // 建立[確認]按鈕
        let okAction = UIAlertAction(
            title: "確認",
            style: .default,
            handler: okAction)
        
        alertController.addAction(okAction)
    
        return alertController
    }
}
