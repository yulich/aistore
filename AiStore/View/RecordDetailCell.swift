//
//  RecordDetailCell.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/5/5.
//  Copyright © 2020 TCIT. All rights reserved.
//

import UIKit

class RecordDetailCell: UITableViewCell {
    
    @IBOutlet weak var labelItem: UILabel!
    @IBOutlet weak var labelCalories: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    
    func bind(model: Receipt) {
        labelItem.text = model.productName
        labelCalories.text = String(model.calories)
        label3.text = String(model.unitPrice)
        label4.text = String(model.quantity)
    }
}
