//
//  MenuCell.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/4/29.
//  Copyright © 2020 TCIT. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    
    @IBOutlet weak var ivIcon: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
}
