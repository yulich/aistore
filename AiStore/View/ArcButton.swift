//
//  BackgroundBtn.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/5/6.
//  Copyright © 2020 TCIT. All rights reserved.
//

import Foundation

import UIKit

class ArcButton: UIButton {

    var shapeLayer: CAShapeLayer? = nil
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        showBackground(rect.width, rect.height)
    }
    
    func showBackground(_ w: CGFloat, _ h: CGFloat) {
        if (shapeLayer == nil) {
            shapeLayer = CAShapeLayer()
            
            shapeLayer!.frame = CGRect(x: 0, y: 0, width: w, height: h)
            shapeLayer!.lineWidth = 0.5
            shapeLayer!.strokeColor = nil
            
            let path = UIBezierPath()
            // 起始角度跟結束角度，最後為是否順時針
            let radius = h / 2
            
            path.addArc(withCenter: CGPoint(x: h / 2, y: radius), radius: radius, startAngle: CGFloat(Float.pi * 0.5), endAngle: CGFloat(Float.pi * 1.5), clockwise: true)
            path.addLine(to: CGPoint(x: w - radius, y: 0))
            path.addArc(withCenter: CGPoint(x: w - h / 2, y: radius), radius: radius, startAngle: CGFloat(Float.pi * 1.5), endAngle: CGFloat(Float.pi * 0.5), clockwise: true)
            path.addLine(to: CGPoint(x: radius, y: h))
            shapeLayer!.path = path.cgPath
            
            layer.insertSublayer(shapeLayer!, at: 0)
        }
    }
}
