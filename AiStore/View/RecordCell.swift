//
//  RecordCell.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/5/2.
//  Copyright © 2020 TCIT. All rights reserved.
//

import UIKit

protocol RecordCellDelegate: class {
    func onClickedDetail(data: BuyListElement)
}

class RecordCell: UITableViewCell {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    
    weak var delegate: RecordCellDelegate?
    
    private var data: BuyListElement?
    
    @IBAction func actionDetail(_ sender: UIButton) {
        if let delegate = delegate, let data = data {
            delegate.onClickedDetail(data: data)
        }
    }
    
    func bind(model: BuyListElement) {
        data = model
        
        if let time = model.time {
            let formatterToData = DateFormatter()
            formatterToData.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            if let date = formatterToData.date(from: time.replacingOccurrences(of: "T", with: " ", options: NSString.CompareOptions.literal, range: nil)) {
                let formatterToString = DateFormatter()
                formatterToString.dateFormat = "yyyy/MM/dd"
                labelDate.text = formatterToString.string(from: date)
            }
        }

        labelAmount.text = String(model.totalPrice)
    }
}
