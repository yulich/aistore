//
//  LineLabel.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/4/30.
//  Copyright © 2020 TCIT. All rights reserved.
//

import UIKit

class LineView: UIView {
    
    var shapeLayer: CAShapeLayer? = nil
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        showLine(rect.width, rect.height)
    }
    
    func showLine(_ x: CGFloat, _ h: CGFloat) {
        if (shapeLayer == nil) {
            shapeLayer = CAShapeLayer()
            
            shapeLayer!.frame = CGRect(x: 0, y: 0, width: x, height: h)
            shapeLayer!.fillColor = nil
            shapeLayer!.lineWidth = 0.5
            shapeLayer!.strokeColor = UIColor.black.cgColor
            
            let path = UIBezierPath()
            path.move(to: CGPoint(x: x, y: 0))
            path.addLine(to: CGPoint(x: x, y: h))
            
            shapeLayer!.path = path.cgPath
            
            layer.addSublayer(shapeLayer!)
        }
    }
}
