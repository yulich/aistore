//
//  LineStackView.swift
//  AiStore
//
//  Created by Jeff Yu on 2020/5/2.
//  Copyright © 2020 TCIT. All rights reserved.
//

import UIKit

class BorderView: UIView {
    
    var shapeLayer: CAShapeLayer? = nil
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        showBorder(rect.width, rect.height)
    }
    
    func showBorder(_ w: CGFloat, _ h: CGFloat) {
        if (shapeLayer == nil) {
            shapeLayer = CAShapeLayer()
            
            shapeLayer!.frame = CGRect(x: 0, y: 0, width: w, height: h)
            shapeLayer!.fillColor = nil
            shapeLayer!.lineWidth = 0.5
            shapeLayer!.strokeColor = UIColor.black.cgColor
            
            let path = UIBezierPath(rect: shapeLayer!.frame)

            shapeLayer!.path = path.cgPath
            
            layer.addSublayer(shapeLayer!)
        }
    }
}
